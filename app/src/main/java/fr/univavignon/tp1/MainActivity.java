package fr.univavignon.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/*public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listview = findViewById(R.id.lstView);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AnimalList.getNameArray());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                //final String item = (String) parent.getItemAtPosition(position);
                //Toast.makeText(MainActivity.this, "Animal sélectionné: " + item, Toast.LENGTH_LONG).show();
                //AnimalInfoActivity();
                Intent animalInfo = new Intent(parent.getContext(), AnimalActivity.class);
                animalInfo.putExtra("name", (String) parent.getItemAtPosition(position));
                startActivity(animalInfo);
            }
        });
    }

    public void AnimalInfoActivity()
    {
        Intent animalInfo = new Intent(this, AnimalActivity.class);
        //animalInfo.putExtra("name", AnimalList.getAnimal());
        startActivity(animalInfo);

    }

}*/

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }

    public class IconicAdapter extends RecyclerView.Adapter<RowHolder>
    {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.layout_row, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position)
        {
            holder.bindModel(AnimalList.getNameArray()[position]);
        }

        @Override
        public int getItemCount()
        {
            return(AnimalList.getNameArray().length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView animalName = null;
        ImageView animalImage = null;



        RowHolder(View row)
        {
            super(row);

            animalName = (TextView) row.findViewById(R.id.label);
            animalImage = (ImageView) row.findViewById(R.id.icon);
            animalName.setOnClickListener(this); // permet d'activer le onClick
            animalImage.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            // Do something in response to the click
            //final String item = animalName.getText().toString();
            //Toast.makeText(v.getContext(), "You selected: " + item, Toast.LENGTH_LONG).show();
            Intent animalInfo = new Intent(MainActivity.this, AnimalActivity.class);
            animalInfo.putExtra("name", (String) animalName.getText());
            startActivity(animalInfo);
        }

        void bindModel(String item)
        {
            animalName.setText(item);
            final Animal animal = AnimalList.getAnimal(item);
            int id = getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());
            animalImage.setImageResource(id);

        }
    }
}