package fr.univavignon.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class AnimalActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        // récupération des informations envoyées depuis la précédente Activity
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");

        final Animal animal = AnimalList.getAnimal(name);


        final TextView animalName = findViewById(R.id.animalName);
        animalName.setText(name);


        ImageView animalImage = findViewById(R.id.animalImage);
        int id = getResources().getIdentifier(animal.getImgFile(),"drawable",getPackageName());
        animalImage.setImageResource(id);



        TextView lifeExperience = findViewById(R.id.lifeExperience);
        TextView lifeExperienceInfo = findViewById(R.id.lifeExperienceInfo);
        lifeExperienceInfo.setText(animal.getStrHightestLifespan());

        TextView gestationPeriod = findViewById(R.id.gestationPeriod);
        TextView gestationPeriodInfo = findViewById(R.id.gestationPeriodInfo);
        gestationPeriodInfo.setText(animal.getStrGestationPeriod());

        TextView birthWeight = findViewById(R.id.birthWeight);
        TextView birthWeightInfo = findViewById(R.id.birthWeightInfo);
        birthWeightInfo.setText(animal.getStrBirthWeight());

        TextView adultWeight = findViewById(R.id.adultWeight);
        TextView adultWeightInfo = findViewById(R.id.adultWeightInfo);
        adultWeightInfo.setText(animal.getStrAdultWeight());

        TextView conservationStatus = findViewById(R.id.conservationStatus);
        final EditText conservationStatusInfo = findViewById(R.id.conservationStatusInfo);
        conservationStatusInfo.setText(animal.getConservationStatus());



        Button saveButton = findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                animal.setConservationStatus(conservationStatusInfo.getText().toString());
                finish();
            }
        });



    }

}
