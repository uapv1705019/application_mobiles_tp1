package fr.univavignon.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class AnimalActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        // récupération des informations envoyées depuis la précédente Activity
        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        Animal animal = AnimalList.getAnimal(name);


        final TextView animalName = (TextView) findViewById(R.id.animalName);
        animalName.setText(name);


        ImageView animalImage = (ImageView) findViewById(R.id.animalImage);

        switch(name)
        {
            case "Ours brun":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.bear));
                break;

            case "Chameau":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.camel));
                break;

            case "Montbéliarde":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.cow));
                break;

            case "Renard roux":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.fox));
                break;

            case "Koala":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.koala));
                break;

            case "Lion":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.lion));
                break;

            case "Panda géant":
                animalImage.setImageDrawable(getResources().getDrawable(R.drawable.panda));
                break;
        }


        //GridView animalInfo = (GridView) findViewById(R.id.animalInfo);
        TextView lifeExperience = (TextView) findViewById(R.id.lifeExperience);
        TextView lifeExperienceInfo = (TextView) findViewById(R.id.lifeExperienceInfo);


        TextView GestationPeriod = (TextView) findViewById(R.id.GestationPeriod);
        TextView GestationPeriodInfo = (TextView) findViewById(R.id.GestationPeriodInfo);


        TextView BirthWeight = (TextView) findViewById(R.id.BirthWeight);
        TextView BirthWeightInfo = (TextView) findViewById(R.id.BirthWeightInfo);


        TextView AdultWeight = (TextView) findViewById(R.id.AdultWeight);
        TextView AdultWeightInfo = (TextView) findViewById(R.id.AdultWeightInfo);


        TextView ConservationStatus = (TextView) findViewById(R.id.ConservationStatus);
        EditText ConservationStatusInfo = (EditText) findViewById(R.id.ConservationStatusInfo);



        Button saveButton = (Button) findViewById(R.id.saveButton);
        //assert secondButton != null;
        saveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });



    }

    /*public void onClick(View view)
    {
        finish();
    }*/
}
