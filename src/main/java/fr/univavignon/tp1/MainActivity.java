package fr.univavignon.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listview = (ListView) findViewById(R.id.lstView);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AnimalList.getNameArray());
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                //final String item = (String) parent.getItemAtPosition(position);
                //Toast.makeText(MainActivity.this, "Animal sélectionné: " + item, Toast.LENGTH_LONG).show();
                //AnimalInfoActivity();
                Intent animalInfo = new Intent(parent.getContext(), AnimalActivity.class);
                animalInfo.putExtra("name", (String) parent.getItemAtPosition(position));
                startActivity(animalInfo);
            }
        });
    }

    /*public void AnimalInfoActivity()
    {
        Intent animalInfo = new Intent(this, AnimalActivity.class);
        //animalInfo.putExtra("name", AnimalList.getAnimal());
        startActivity(animalInfo);

    }*/



}
